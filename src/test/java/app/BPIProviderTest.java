package app;

import org.junit.Assert;
import org.junit.Test;

public class BPIProviderTest {

    private final IBPIInfoProvider bitcoinInfoProvider = new BPIInfoProvider();

    @Test
    public void testCurrencyNotSupported() {
        testInvalidCurrency("wrong_currency");
        testInvalidCurrency("");
        testInvalidCurrency(null);
    }

    private void testInvalidCurrency(String currency) {
        try {
            BPIInfo bpiInfo = bitcoinInfoProvider.getBPIInfo(currency);
            Assert.fail("Wrong currency is supported");
        } catch (Exception e) {
            Assert.assertEquals(CurrencyIsNotSupportedException.class, e.getClass());
        }
    }

    @Test
    public void testCustomCurrency() {
        testValidCurrency("eur");
        testValidCurrency("  Gbp   ");
    }

    private void testValidCurrency(String currency) {
        try {
            BPIInfo bpiInfo = bitcoinInfoProvider.getBPIInfo(currency);
            Assert.assertNotNull(bpiInfo);
            Assert.assertEquals(currency.trim().toUpperCase(), bpiInfo.getCurrency());
            Assert.assertTrue(bpiInfo.getCurrentRate() >= 0);
            Assert.assertTrue(bpiInfo.getMinRate() >= 0);
            Assert.assertTrue(bpiInfo.getMaxRate() >= 0);
        } catch (Exception e) {
            Assert.fail("Currency should be supported");
        }
    }

}
