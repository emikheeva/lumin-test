package app;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Main {

    public static void main(String[] args) throws IOException {
        System.out.println("Welcome to Bitcoin Price Index real-time data!");
        System.out.println("To get BPI info please insert the currency: ");

        //reading currency
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String currency = reader.readLine();

        IBPIInfoProvider bitcoinInfoProvider = new BPIInfoProvider();
        try {
            //requesting BPI info
            System.out.println(bitcoinInfoProvider.getBPIInfo(currency));
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }
}
