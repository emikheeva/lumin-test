package app;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.*;

public class BPIInfoProvider implements IBPIInfoProvider {

    private final static String CURRENT_RATE_REQUEST_T = "https://api.coindesk.com/v1/bpi/currentprice/%s.json";
    private final static String HISTORY_RATE_REQUEST_T = "https://api.coindesk.com/v1/bpi/historical/close.json?start=%tF&end=%tF&currency=%s";

    private static void getCurrentRate(String currency, BPIInfo outBitcoinInfo) throws IOException {
        String response = sendRequest(String.format(CURRENT_RATE_REQUEST_T, currency));

        //parse current rate response
        ObjectMapper mapper = new ObjectMapper();
        JsonNode json = mapper.readValue(response, JsonNode.class);
        double rate = json.get("bpi").get(currency).get("rate_float").asDouble();

        outBitcoinInfo.setCurrentRate(rate);
    }

    private static void getPrevMinMaxRates(String currency, BPIInfo outBitcoinInfo) throws IOException {
        Calendar calendar = new GregorianCalendar();
        Date today = calendar.getTime();
        calendar.add(Calendar.DAY_OF_YEAR, -30);
        Date prev30Days = calendar.getTime();

        String response = sendRequest(String.format(HISTORY_RATE_REQUEST_T, prev30Days, today, currency));

        //parse history response
        ObjectMapper mapper = new ObjectMapper();
        JsonNode json = mapper.readValue(response, JsonNode.class);
        JsonNode bpi = json.get("bpi");

        //find min and max values
        Iterator<Map.Entry<String, JsonNode>> bpiIt = bpi.fields();
        if (bpiIt.hasNext()) {
            double min = Double.MAX_VALUE;
            double max = 0;

            while (bpiIt.hasNext()) {
                double currentBpi = bpiIt.next().getValue().asDouble();
                if (currentBpi < min) {
                    min = currentBpi;
                }
                if (currentBpi > max) {
                    max = currentBpi;
                }
            }

            outBitcoinInfo.setMaxRate(max);
            outBitcoinInfo.setMinRate(min);
        }
    }

    private static String sendRequest(String urlStr) throws IOException {
        URL url = new URL(urlStr);
        HttpURLConnection con = (HttpURLConnection) url.openConnection();
        StringBuilder content = new StringBuilder();

        try (BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()))) {
            String inputLine;
            while ((inputLine = in.readLine()) != null) {
                content.append(inputLine);
            }
        }

        con.disconnect();
        return content.toString();
    }

    @Override
    public BPIInfo getBPIInfo(String currency) throws Exception {
        if (currency == null) {
            throw new CurrencyIsNotSupportedException(currency);
        }

        currency = currency.trim().toUpperCase();
        BPIInfo bitcoinInfo = new BPIInfo(currency);

        try {
            getCurrentRate(currency, bitcoinInfo);
            getPrevMinMaxRates(currency, bitcoinInfo);
            return bitcoinInfo;
        } catch (IOException e) {
            throw new CurrencyIsNotSupportedException(currency);
        }
    }

}
