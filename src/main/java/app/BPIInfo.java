package app;

public class BPIInfo {
    private final String currency;
    private double currentRate;
    private double minRate;
    private double maxRate;

    public BPIInfo(String currency) {
        this.currency = currency;
    }

    public double getCurrentRate() {
        return currentRate;
    }

    public void setCurrentRate(double currentRate) {
        this.currentRate = currentRate;
    }

    public double getMaxRate() {
        return maxRate;
    }

    public void setMaxRate(double maxRate) {
        this.maxRate = maxRate;
    }

    public double getMinRate() {
        return minRate;
    }

    public void setMinRate(double minRate) {
        this.minRate = minRate;
    }

    public String getCurrency() {
        return currency;
    }

    @Override
    public String toString() {
        return String.format("Current rate: %s %s\n" +
                "Lowest rate in the last 30 days: %s %s\n" +
                "Highest rate in the last 30 days: %s %s\n",
                currentRate, currency, minRate, currency, maxRate, currency);
    }
}
