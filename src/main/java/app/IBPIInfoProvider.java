package app;

public interface IBPIInfoProvider {

    BPIInfo getBPIInfo(String currency) throws Exception;

}
