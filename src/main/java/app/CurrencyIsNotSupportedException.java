package app;

public class CurrencyIsNotSupportedException extends Exception {
    private final static String CURRENCY_NOT_SUPPORTED_T = "Currency \"%s\" is not supported";

    public CurrencyIsNotSupportedException(String currency) {
        super(String.format(CURRENCY_NOT_SUPPORTED_T, currency));
    }
}
