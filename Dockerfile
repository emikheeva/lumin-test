FROM openjdk

ADD out/artifacts/lumin_test_jar/lumin-test.jar lumin-test.jar
CMD java -jar lumin-test.jar